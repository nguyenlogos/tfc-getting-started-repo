
resource "aws_s3_bucket" "my-s3-bucket" {
  bucket = "s3-terraform-tesing-002"
  acl = "private"
   versioning {
    enabled = true
  } 
}